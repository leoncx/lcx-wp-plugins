<?php
/*  Copyright 2012  Maximilien Bersoult  (email : leoncx@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once ABSPATH . 'wp-admin/includes/upgrade.php';

/**
 * Abstract class for database for lcx-mmo* plugins
 *
 * @author Maximilien Bersoult <leoncx@gmail.com>
 * @version 1.0.0
 * @package lcx-mmo
 * @license GPLv2
 * @copyright 2012
 */
abstract class LcxMmo_DbAbstract
{
    protected $_wpdb = null;
    protected $_dbVersion = '1.0.0';
    protected $_dbName = 'lcx-mmo';
    protected $_tables = array();
    
    /**
     * Constructor
     *
     * @param wpdb $wpdb The database object
     */
    public function __construct($wpdb)
    {
        $this->_wpdb = $wpdb;
    }

    /**
     * Install the database plugin
     */
    public function install()
    {
        $this->runTables();
        add_option($this->_dbName, $this->_dbVersion);
    }

    /**
     * Update the database plugin
     */
    public function update()
    {
        $installedVersion = get_option($this->_dbName);
	
	if ($installedVersion == $this->_dbVersion) {
	    return;
	}
	$this->runTables;
        update_option($this->_dbName, $this->_dbVersion);
    }

    /**
     * Uninstall the database plugin
     */
    public function uninstall()
    {
        foreach ($this->_tables as $table => $sql) {
	    $query = 'DROP TABLE IF EXISTS ' . $this->_wpdb->prefix . $table;
	    $this->_wpdb->query($query);
	}
	delete_option($this->_dbName);
    }

    /**
     * Update data
     *
     * @todo
     */
    public function updateData()
    {
    }

    /**
     * Execute the create tables query
     */
    protected function runTables()
    {
        foreach ($this->_tables as $table => $sql) {
	    $sql = str_replace('@TABLE_NAME@', $this->_wpdb->prefix . $table, $sql);
	    dbDelta($sql);
	}
    }

    /**
     * Insert data
     */
    abstract public function insertData();
}
