<?php
/*  Copyright 2012  Maximilien Bersoult  (email : leoncx@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once LCX_MMO_LIB . '/LcxMmo/DbAbstract.php';

/**
 * Class for database for lcx-mmo
 * 
 * @author Maximilien Bersoult <leoncx@gmail.com>
 * @version 1.0.0
 * @package lcx-mmo
 * @license GPLv2
 * @copyright 2012
 */
class LcxMmo_Db extends LcxMmo_DbAbstract
{
    protected $_dbVersion = '1.0.0';
    protected $_dbName = 'lcx-mmo-db';
    protected $_tables = array(
        'lcx_games' => 'CREATE TABLE @TABLE_NAME@ (
	    game_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	    shortname VARCHAR(50) NOT NULL,
	    name VARCHAR(255) NOT NULL,
	    publisher VARCHAR(255),
            PRIMARY KEY(game_id),
	    UNIQUE KEY @TABLE_NAME@_idx01 (shortname)
	)'
    );

    /**
     * Insert data
     */
    public function insertData()
    {
        $this->_wpdb->insert($this->_wpdb->prefix . 'lcx_games', array(
	    'shortname' => 'wow',
	    'name' => 'World Of Warcraft',
	    'publisher' => 'Blizzard Entertainment'
	), array('%s', '%s', '%s'));
        $this->_wpdb->insert($this->_wpdb->prefix . 'lcx_games', array(
	    'shortname' => 'rift',
	    'name' => 'Rift',
	    'publisher' => 'Trion Worlds, Inc'
	), array('%s', '%s', '%s'));
        $this->_wpdb->insert($this->_wpdb->prefix . 'lcx_games', array(
	    'shortname' => 'swtor',
	    'name' => 'Star Wars : The old republic',
	    'publisher' => 'Bioware'
	), array('%s', '%s', '%s'));
        $this->_wpdb->insert($this->_wpdb->prefix . 'lcx_games', array(
	    'shortname' => 'tera',
	    'name' => 'Tera',
	    'publisher' => 'Bluehole studio'
	), array('%s', '%s', '%s'));
    }
}
