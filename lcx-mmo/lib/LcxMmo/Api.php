<?php
/*  Copyright 2012  Maximilien Bersoult  (email : leoncx@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

/**
 * The API for lcx-mmo
 *
 * @author Maximilien Bersoult <leoncx@gmail.com>
 * @version 1.0.0
 * @package lcx-mmo
 * @license GPLv2
 * @copyright 2012
 */
class LcxMmo_Api
{
    static private $_instance = null;
    private $_wpdb = null;
    private $_cacheGames = null;

    /**
     * Constructor
     *
     * @param wpdb $wpdb The database object
     */
    private function __construct($wpdb)
    {
        $this->_wpdb = $wpdb;
    }

    /**
     * Get the list of games
     *
     * @return the list of games
     */
    public function listGames()
    {
        if (!is_null($this->_cacheGames)) {
	    return $this->_cacheGames;
	}

	$this->_cacheGames = $this->_wpdb->get_results('SELECT game_id, shortname, name
	    FROM ' .  $this->_wpdb->prefix . 'lcx_games 
	    ORDER BY shortname', ARRAY_A);

	return $this->_cacheGames;
    }

    /**
     * Add a game
     *
     * @param string $shortname The shortname of game
     * @param string $name The name of game
     * @param string $publisher The publisher of game
     */
    public function addGame($shortname, $name, $publisher = null)
    {
        $values = array(
	    'shortname' => $shortname,
	    'name' => $name
	);
	$types = array('%s', '%s');
	if (!is_null($publisher)) {
	    $values['publisher'] = $publisher;
	    $types[] = '%s';
	}
	$this->_wpdb->insert($this->_wpdb->prefix . 'lcx_games', $values, $types);
	$this->_cacheGames = null;
    }

    /**
     * Get instance for API
     *
     * @param wpdb $wpdb The database object
     */
    static public function getInstance($wpdb = null)
    {
        if (is_null(self::$_instance) && is_null($wpdb)) {
	    throw new Exception('');
	}
	if (is_null(self::$_instance)) {
	    self::$_instance = new LcxMmo_Api($wpdb);
	}
	return self::$_instance;
    }
}
