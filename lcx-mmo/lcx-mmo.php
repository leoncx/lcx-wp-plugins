<?php
/*
Plugin Name: Lcx MMO
Plugin URI: https://bitbucket.org/leoncx/lcx-wp-plugins/overview
Description: Base plugins for lcx-mmo*
Version: 1.0.0
Author: Maximilien Bersoult
Author URI: https://bitbucket.org/leoncx/
License: GPL2
*/
/*  Copyright 2012  Maximilien Bersoult  (email : leoncx@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

define('LCX_MMO_LIB', dirname(__FILE__) . '/lib');

require_once LCX_MMO_LIB . '/LcxMmo/Db.php';
require_once LCX_MMO_LIB . '/LcxMmo/Api.php';

$lcxmmodb = new LcxMmo_Db($wpdb);

register_activation_hook(__FILE__, array(&$lcxmmodb, 'install'));
register_activation_hook(__FILE__, array(&$lcxmmodb, 'insertData'));
register_deactivation_hook(__FILE__, array(&$lcxmmodb, 'uninstall'));
add_action('plugins_loaded', array(&$lcxmmodb, 'update'));
add_action('plugins_loaded', array(&$lcxmmodb, 'updateData'));

LcxMmo_Api::getInstance($wpdb);
